package com.example.demo.constants;

public interface MappingNames {
    String BackendRoot = "/web-api";
    String User = "/user";
    String Role = "/role";
    String Currency = "/currency";
    String Footbal = "/football";


    String GET_CURRENCY = "/latest";
    String GET_PLAYER_STATS = "/";
}
