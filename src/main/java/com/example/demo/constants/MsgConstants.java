package com.example.demo.constants;

public interface MsgConstants {
    String USER = "user";
    String SUCCESSFUL = "common.successful";
    String INCORRECT_INCOMING_PARAMS = "backend.incorrectparam";
    String SERVER_ERROR = "server.error";
    String EMAIL_IS_EXISTS = "email.exists";
    String PHONE_IS_EXISTS = "phone.exists";
    String USER_ALREADY_DELETED = "user.alreadyDeleted";
    String USER_NOT_FOUND = "user.not.found";
}
