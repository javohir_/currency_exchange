package com.example.demo.constants;

public interface TableNames {
    String User = "users";
    String Role = "roles";
}
