package com.example.demo.controller;

import com.example.demo.constants.MappingNames;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(name = MappingNames.BackendRoot + MappingNames.Role)
@Data
public class RoleController {

}
