package com.example.demo.controller;

import com.example.demo.constants.MappingNames;
import com.example.demo.constants.MsgConstants;
import com.example.demo.controller.base.BaseController;
import com.example.demo.dto.UserDto;
import com.example.demo.dto.shared.ResponseItem;
import com.example.demo.services.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Data
@RestController
@RequestMapping(value = MappingNames.BackendRoot + MappingNames.User, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController extends BaseController {

    @Qualifier("userService")
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(
            name = "register user",
            value = "/registerUser",
            method = RequestMethod.POST
    )
    public ResponseEntity<ResponseItem<Object>> createUser(@RequestBody UserDto userDto){
        return success( userService.createUser(userDto));
    }

    @RequestMapping(
            name = "get users",
            value = "/getUsers",
            method = RequestMethod.GET
    )
    public ResponseEntity<ResponseItem<Object>> getUsers(){
        return success(userService.getUser());
    }

    @RequestMapping(
            name = "get active users",
            value = "/getActiveUsers",
            method = RequestMethod.GET
    )
    public ResponseEntity<ResponseItem<Object>> getActiveUsers(){
        return success(userService.getActiveUsers());
    }

    @RequestMapping(
            name = "delete user by id",
            value = "/deleteUser",
            method = RequestMethod.DELETE
    )
    public ResponseEntity<ResponseItem<Object>> deleteUser(@RequestParam Long id){
        return success(userService.deleteUser(id));
    }

    @RequestMapping(
            name = "get deleted users",
            value = "/getDeletedUsers",
            method = RequestMethod.GET
    )
    public ResponseEntity<ResponseItem<Object>> getDeletedUsers(){
        return success(userService.getDeletedUsers());
    }

}
