package com.example.demo.controller.base;

import com.example.demo.constants.MappingNames;
import com.example.demo.dto.shared.ResponseItem;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseController{
    private <T> ResponseEntity<ResponseItem<T>> send(T data, String message, String status, HttpStatus httpStatusCode){
        return new ResponseEntity<>(new ResponseItem<>(data, message, null, status, httpStatusCode.value()),
                getHttpHeaders(), httpStatusCode);
    }

    protected   <T> ResponseEntity<ResponseItem<T>> success(T data){
        return send(data, null, Status.SUCCESS.name(), HttpStatus.OK);
    }

//    protected <T> ResponseEntity<ResponseItem<T>> fail(T data, String message){
//        return this.send(data, message, Status.FAIL.name(), HttpStatus.BAD_REQUEST);
//    }
//    protected <T> ResponseEntity<ResponseItem<T>> error(String message){
//        return this.send(null, null, Status.ERROR.name(), HttpStatus.BAD_REQUEST);
//    }
    private HttpHeaders getHttpHeaders(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json; charset=utf-8");
        return httpHeaders;
    }
    private enum Status{
        SUCCESS, FAIL, ERROR
    }
}
