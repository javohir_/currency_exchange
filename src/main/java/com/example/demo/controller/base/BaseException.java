package com.example.demo.controller.base;

import com.example.demo.constants.MsgConstants;
import com.example.demo.dto.shared.ResponseItem;
import com.example.demo.exception.MyException;
//import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BaseException{
    private static final String STATUS_FAIL = "FAIL";
    private static final String STATUS_ERROR = "ERROR";

    @ExceptionHandler(MyException.class)
    public ResponseEntity<ResponseItem> baseControllerException(MyException myException){
        return getResponse(STATUS_FAIL, HttpStatus.FORBIDDEN, myException.getMessage(), MsgConstants.SERVER_ERROR, myException.getData());
    }
//    @ExceptionHandler(MyException.class)
//    ResponseEntity<ResponseItem> baseError(MyException myException){
//        return getResponse(STATUS_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, myException.getMessage(), MsgConstants.SERVER_ERROR, myException.getData());
//    }
    public ResponseEntity<ResponseItem> getResponse(String statusMessage, HttpStatus httpStatus,
                                                    String message, String serverMessage, Object data){
        ResponseItem<Object> item = new ResponseItem<>();
        item.setStatus(statusMessage);
        item.setServerMessage(serverMessage);
        item.setData(data);
        item.setHttpStatusCode(httpStatus.value());
        item.setMessage(message);
        return ResponseEntity.status(HttpStatus.OK).body(item);
    }

}
