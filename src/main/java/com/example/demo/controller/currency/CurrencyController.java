package com.example.demo.controller.currency;

import com.example.demo.constants.MappingNames;
import com.example.demo.controller.base.BaseController;
import com.example.demo.services.currency.CurrencyService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = MappingNames.BackendRoot + MappingNames.Currency, produces = MediaType.APPLICATION_JSON_VALUE)
public class CurrencyController extends BaseController {

    private final CurrencyService currencyService;
    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }
    @RequestMapping(value = MappingNames.GET_CURRENCY, method = RequestMethod.GET)
    public ResponseEntity<?> latestCurrency(){
        return success(currencyService.latest());
    }
}
