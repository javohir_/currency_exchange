package com.example.demo.dto;

import lombok.Data;

@Data
public class RoleDto {

   private String name;
   private String code;

   public RoleDto(){
   }
}
