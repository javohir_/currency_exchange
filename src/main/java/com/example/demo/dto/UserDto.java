package com.example.demo.dto;

//import com.example.demo.entities.User;
import lombok.Data;

@Data
public class UserDto {
    private String fullname;
    private String username;
    private String password;
    private String phoneNumber;
    private String email;

    public UserDto() {
    }
}
