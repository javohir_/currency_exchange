package com.example.demo.dto.currency;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rates {
    @JsonProperty("USD")
    private Double USD;
    @JsonProperty("GBP")
    private Double GBP;
}
