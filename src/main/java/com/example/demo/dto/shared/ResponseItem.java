package com.example.demo.dto.shared;

import lombok.Data;
//import lombok.RequiredArgsConstructor;

import java.io.Serializable;
@Data
public class ResponseItem<T> implements Serializable {
   private T data;
   private String message;
   private String serverMessage;
   private String status;
   private int  httpStatusCode;


   public ResponseItem() {
   }

   public ResponseItem(T data, String message, String serverMessage, String status, int httpStatusCode) {
      this.data = data;
      this.message = message;
      this.serverMessage = serverMessage;
      this.status = status;
      this.httpStatusCode = httpStatusCode;
   }

}
