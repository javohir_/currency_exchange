package com.example.demo.entities;

import ch.qos.logback.classic.db.names.TableName;
import com.example.demo.constants.TableNames;
import com.example.demo.entities.base.DataEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = TableNames.Role)
@Data
public class Role extends DataEntity {
    private String name;
    private String code;
}
