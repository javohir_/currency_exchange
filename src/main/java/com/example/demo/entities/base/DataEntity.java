package com.example.demo.entities.base;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

@Data
@MappedSuperclass
public  abstract class DataEntity extends BaseEntity{

    @Column(nullable = false)
    private boolean deleted;


    @Column(nullable = false)
    private Date createdDate;


    @Column(nullable = false)
    private Date modifiedDate;



    protected DataEntity(){
        this.deleted = false;
    }

    @PreUpdate
    public void preUpdate(){
        this.setModifiedDate(new Date(System.currentTimeMillis()));
    }

    @PrePersist
    public void prePersist(){
        if( this.isNew() || this.getCreatedDate() == null){
            this.setCreatedDate(new Date(System.currentTimeMillis()));
        }
        this.setModifiedDate(new Date(System.currentTimeMillis()));
    }

}
