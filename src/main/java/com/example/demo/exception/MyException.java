package com.example.demo.exception;

import lombok.Data;

@Data
public class MyException extends RuntimeException {
    private String message;
    private Object data;

    public MyException(String message) {
        super(message);
        this.message = message;
    }

}