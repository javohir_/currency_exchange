package com.example.demo.repos;
import com.example.demo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserById(Long id);
   @Query(
           "select count(u.email) from User u" +
                   " where u.deleted = false " +
                   "and lower(u.email) = ?1 "
   )
    Long emailExists(String email);

   @Query(
           "select count(p.phoneNumber) from User p"
   )
    Long phoneExists(String phone);

   @Query(
           "select u from User u" +
                   " where u.deleted = false"
   )
    List<User> getActiveUsers();
   @Query(
           "select du from User du" +
                   " where du.deleted = true "
   )
   List<User> getDeletedUsers();
}
