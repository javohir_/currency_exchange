package com.example.demo.services;

import com.example.demo.dto.UserDto;
import com.example.demo.entities.User;
import java.util.List;

public interface UserService {
    String createUser(UserDto userDto);
    List<User> getUser();
    String deleteUser(Long id);
    List<User> getActiveUsers();
    List<User> getDeletedUsers();

}
