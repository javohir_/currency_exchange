package com.example.demo.services.currency;

import com.example.demo.dto.currency.CurrencyItem;
import org.springframework.http.ResponseEntity;

public interface CurrencyService {
    CurrencyItem latest();

}
