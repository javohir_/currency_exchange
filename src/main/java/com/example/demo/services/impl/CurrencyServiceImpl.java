package com.example.demo.services.impl;

import com.example.demo.dto.currency.CurrencyItem;
import com.example.demo.services.currency.CurrencyService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CurrencyServiceImpl implements CurrencyService {
    private final RestTemplate restTemplate;
    private static final String URL = "https://api.exchangeratesapi.io/latest";
//    private static final String URL = "https://api.exchangeratesapi.io/latest?symbols=USD,GBP";

    public CurrencyServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public CurrencyItem latest() {

        ResponseEntity<CurrencyItem> currencyItemResponseEntity = null;
        try {
            currencyItemResponseEntity = restTemplate.exchange(URL,
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<CurrencyItem>() {});
        } catch (Exception e) {

        }
        CurrencyItem currencyItem1 = currencyItemResponseEntity.getBody();

        return currencyItem1;
    }


}
