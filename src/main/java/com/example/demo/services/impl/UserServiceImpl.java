package com.example.demo.services.impl;

import com.example.demo.constants.MsgConstants;
import com.example.demo.controller.base.BaseController;
import com.example.demo.dto.UserDto;
import com.example.demo.entities.User;
import com.example.demo.exception.MyException;
import com.example.demo.repos.UserRepository;
import com.example.demo.services.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl extends BaseController implements UserService, UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null;
    }

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String createUser(UserDto userDto) {
        if (userDto.equals(null)){
            throw new MyException(MsgConstants.INCORRECT_INCOMING_PARAMS);
        }

        if (userDto.getEmail().isEmpty()){
            throw new MyException(MsgConstants.INCORRECT_INCOMING_PARAMS);
        }else if (userRepository.emailExists(userDto.getEmail()) > 0){
            throw new MyException(MsgConstants.EMAIL_IS_EXISTS);
        }

        if (userRepository.phoneExists(userDto.getPhoneNumber()) > 0){
            throw new MyException(MsgConstants.PHONE_IS_EXISTS);
        }

        User user = new User();
        user.setFullName(userDto.getFullname());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setEmail(userDto.getEmail());
        user.setPhoneNumber(userDto.getPhoneNumber());

        this.userRepository.save(user);
        return MsgConstants.SUCCESSFUL;
    }

    @Override
    public List<User> getUser() {
        return userRepository.findAll();
    }

    @Override
    public String deleteUser(Long id) {
        if (id < 0 && id.equals(null)){
            throw  new MyException(MsgConstants.INCORRECT_INCOMING_PARAMS);
        }
        final User user = userRepository
               .findUserById(id)
               .orElseThrow(()-> new MyException(MsgConstants.USER_NOT_FOUND));

        if (user.isDeleted()){
            throw  new MyException(MsgConstants.USER_ALREADY_DELETED);
        }
        user.setDeleted(true);
        userRepository.save(user);
        return MsgConstants.SUCCESSFUL;
    }

    @Override
    public List<User> getActiveUsers() {
        return userRepository.getActiveUsers();
    }

    @Override
    public List<User> getDeletedUsers() {
        return userRepository.getDeletedUsers();
    }
}
